/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.*;

/**
 *
 * @author root
 */
public class JogadorComparator implements Comparator<Jogador> {
    
    private int tipo;
    
    public JogadorComparator(){
        //Ordenar por numero e nome em ordem ascendente
        tipo=2;
    }
    
    public JogadorComparator(boolean b1, boolean b2, boolean b3){
        if (b1==true)
            //Ordenacao por numero
            tipo=1;
        if (b2==true)
            //Ordenacao por numero ascendente (maior)
            tipo=2;
        if (b3==true)
            //Ordenacao por NOME ascendente (maior)
            tipo=3;
    }

    @Override
    public int compare(Jogador o1, Jogador o2) {
        int result=0;
        //Ordenacao por numero
        if (tipo==1){
            
            if (o1.numero < o2.numero)
                result = o1.numero;
            else
                if (o1.numero == o2.numero)
                    result = o1.nome.compareTo(o2.nome);
                
        }            

        //Numero ascendente
        if (tipo==2){        
            
            if (o1.numero > o2.numero)
                result = o1.numero;
            else
                if (o1.numero == o2.numero)
                    result = o1.nome.compareTo(o2.nome);
        } 
        
        //Nome ascendente
        if (tipo==3) {
            result = o1.nome.compareTo(o2.nome);
            if (result==0)     
                result = o1.numero - o2.numero;

        }
        return result;
    }
    
}

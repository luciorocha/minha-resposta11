
import utfpr.ct.dainf.if62c.pratica.*;
import java.util.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {

    public static void main(String[] args) {

        Time time1 = new Time();
        time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
        time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
        time1.addJogador("Lateral1", new Jogador(4, "Ciclano1"));
        time1.addJogador("Atacante", new Jogador(10, "Beltrano"));

        JogadorComparator j = new JogadorComparator(false, true, false);

        List<Jogador> list = time1.ordena(j);
        int i = 0;
        while (i < list.size()) {
            System.out.println(list.get(i++));
        }

        //Para o binarySearch
        Collections.sort(list);
        i = 0;
        while (i < list.size()) {
            System.out.println(list.get(i++));
        }

        int p = 1;
        p = Collections.binarySearch(list,new Jogador(10,"Beltrano"));

        System.out.println("Posicao: " + p);
        System.out.println(list.get(p));

    }
}
